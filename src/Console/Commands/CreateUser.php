<?php

namespace WTP\Microservices\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for creation new user';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $create = [
            'name' => $this->ask('Name: '),
            'email' => $this->ask('Email: '),
            'password' => $this->ask('Password: ')
        ];

        $validator = Validator::make($create, [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password'  => 'required|min:3',
        ]);
        if ($validator->fails()) {
            foreach ($validator->errors()->toArray() as $error) { // Validation errors
                $this->error("Error: {$error[0]}");
            }
            return Command::FAILURE;
        }

        $user = User::create($create);

        if ($user) {
            $this->info('User created:');
            $this->comment('name: ' . $user->name);
            $this->comment('email: ' . $user->email);
            $this->comment('password: ' . $create['password']);
        } else {
            $this->error('User can not be created');
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
