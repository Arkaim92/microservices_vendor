<?php

namespace WTP\Microservices\DataModels;

use Illuminate\Support\Carbon;

/**
 * Class Deposit
 * A DTO object for representing an array of received data as an object with properties.
 * @package WTP\Microservices\Services\Rest
 *
 * @property string id
 * @property string customer_id
 * @property string wallet_id кошелек с которого создается депозит, на него же возвращается при закрытии
 * @property string currency_id валюта
 * @property float invested начальная сумма депозита
 * @property float balance текущий баланс (с учетом начислений)
 * @property float rate
 * @property float percent
 * @property float speed_amount
 * @property float full_speed_amount in PZM
 * @property float full_speed_amount_acc in GNT
 * @property bool active статус
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Carbon accrued_at
 * @property Carbon speeded_at
 */
class Deposit
{
    public function __construct($data)
    {
        if (!is_array($data)) {
            $data = (array)$data;
        }

        $this->id = $data['id'];
        $this->customer_id = $data['customer_id'];
        $this->wallet_id = $data['wallet_id'];
        $this->currency_id = $data['currency_id'];
        $this->invested = (float)$data['invested'];
        $this->balance = (float)$data['balance'];
        $this->percent = (float)$data['percent'];
        $this->speed_amount = (float)$data['speed_amount'];
        $this->full_speed_amount = (float)$data['full_speed_amount'];
        $this->full_speed_amount_acc = (float)$data['full_speed_amount_acc'];
        $this->active = (bool)$data['active'];
        $this->rate = (float)($data['rate'] ?? null);
        $this->accrued_at = Carbon::create($data['accrued_at']);
        $this->speeded_at = empty($data['speeded_at']) ? null : Carbon::create($data['speeded_at']);
        $this->created_at = Carbon::create($data['created_at']);
        $this->updated_at = Carbon::create($data['updated_at']);
    }
}
