<?php

namespace WTP\Microservices\DataModels;

use Illuminate\Support\Carbon;

/**
 * Class Transaction
 * @package WTP\Microservices\Services\Rest
 *
 * @property string id
 * @property string type_id - тип операции
 * @property string customer_id
 * @property string rate_id - тарифный план, если это депозитная транзакция.
 * @property string deposit_id
 * @property string wallet_id
 * @property string payment_system_id
 * @property string order_id
 * @property float amount
 * @property float commission
 * @property float rate
 * @property string source - кошелек реферала пользователя, если это партнерская транзакция.
 * @property string result - ответ платежной системы.
 * @property string batch_id - ИД операции в платежной системе.
 * @property string confirmation_code
 * @property bool approved
 * @property int check
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Carbon confirmation_sent_at
 */
class Transaction
{
    public function __construct($data)
    {
        if (!is_array($data)) {
            $data = (array)$data;
        }

        $this->id = $data['id'];
        $this->type_id = $data['type_id'];
        $this->customer_id = $data['customer_id'];
        $this->rate_id = $data['rate_id'] ?? null;
        $this->deposit_id = $data['deposit_id'] ?? null;
        $this->wallet_id = $data['wallet_id'] ?? null;
        $this->order_id = $data['order_id'] ?? null;
        $this->payment_system_id = $data['payment_system_id'] ?? null;
        $this->amount = (float)$data['amount'];
        $this->commission = (float)($data['commission'] ?? null);
        $this->rate = (float)($data['rate'] ?? null);
        $this->source = $data['source'];
        $this->result = $data['result'];
        $this->batch_id = $data['batch_id'];
        $this->confirmation_code = $data['confirmation_code'] ?? null;
        $this->approved = (bool)$data['approved'];
        $this->check = (int)$data['check'];
        $this->created_at = empty($data['created_at'])
            ? null : Carbon::create($data['created_at']);
        $this->updated_at = empty($data['updated_at'])
            ? null : Carbon::create($data['updated_at']);
        $this->confirmation_sent_at = empty($data['confirmation_sent_at'])
            ? null : Carbon::create($data['confirmation_sent_at']);
    }
}
