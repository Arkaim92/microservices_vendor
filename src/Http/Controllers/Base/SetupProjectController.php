<?php

namespace WTP\Microservices\Http\Controllers\Base;

use App\Http\Controllers\Controller;
use WTP\Microservices\Http\Requests\SetupProject\RequestSetupValidation;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;

/**
 * Class SetupProjectController
 * @package App\Http\Controllers\Api\V1
 */
class SetupProjectController extends Controller
{
    private RequestSetupValidation $validation;

    public function __construct(RequestSetupValidation $validation)
    {
        if (env('SETUP_LOCK', 0) == 1) {
            die('SETUP LOCKED');
        }
        $this->validation = $validation;
    }

    /**
     * @OA\Post(
     *     path="/setup/env/db",
     *     tags={"setup"},
     *     description="Env for database",
     *     security={{"bearer":{}}},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *           mediaType="application/x-www-form-urlencoded",
     *           @OA\Schema(
     *               type="object",
     *               @OA\Property(
     *                  property="DB_CONNECTION",
     *                  type="string"
     *               ),
     *               @OA\Property(
     *                  property="DB_HOST",
     *                  type="string"
     *               ),
     *               @OA\Property(
     *                  property="DB_PORT",
     *                  type="string"
     *               ),
     *               @OA\Property(
     *                  property="DB_DATABASE",
     *                  type="string"
     *               ),
     *               @OA\Property(
     *                  property="DB_USERNAME",
     *                  type="string"
     *               ),
     *               @OA\Property(
     *                  property="DB_PASSWORD",
     *                  type="string"
     *               ),
     *           ),
     *       )
     *   ),
     *     @OA\Response(
     *         response=200,
     *         description="response",
     *         @OA\JsonContent(
     *             title="response",
     *             type="object",
     *             example={},
     *         )
     *     ),
     * )
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function envdb(Request $request)
    {
        $this->validate($request, $this->validation->envdb());

        $this->writeNewEnvironmentFileWith('DB_CONNECTION', $request->get('DB_CONNECTION'));
        $this->writeNewEnvironmentFileWith('DB_HOST', $request->get('DB_HOST'));
        $this->writeNewEnvironmentFileWith('DB_PORT', $request->get('DB_PORT'));
        $this->writeNewEnvironmentFileWith('DB_DATABASE', $request->get('DB_DATABASE'));
        $this->writeNewEnvironmentFileWith('DB_USERNAME', $request->get('DB_USERNAME'));
        $this->writeNewEnvironmentFileWith('DB_PASSWORD', $request->get('DB_PASSWORD'));

        return response()->json(['status' => 'ok'], Response::HTTP_OK);
    }

    /**
     * @OA\Post(
     *     path="/setup/env/memcache",
     *     tags={"setup"},
     *     description="Env for memcache",
     *     security={{"bearer":{}}},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *           mediaType="application/x-www-form-urlencoded",
     *           @OA\Schema(
     *               type="object",
     *               @OA\Property(
     *                  property="MEMCACHED_PERSISTENT_ID",
     *                  type="string"
     *               ),
     *               @OA\Property(
     *                  property="MEMCACHED_USERNAME",
     *                  type="string"
     *               ),
     *               @OA\Property(
     *                  property="MEMCACHED_PASSWORD",
     *                  type="string"
     *               ),
     *               @OA\Property(
     *                  property="MEMCACHED_HOST",
     *                  type="string"
     *               ),
     *               @OA\Property(
     *                  property="MEMCACHED_PORT",
     *                  type="string"
     *               ),
     *           ),
     *       )
     *   ),
     *     @OA\Response(
     *         response=200,
     *         description="response",
     *         @OA\JsonContent(
     *             title="response",
     *             type="object",
     *             example={},
     *         )
     *     ),
     * )
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|Response
     */
    public function envmemcache(Request $request)
    {
        $this->validate($request, $this->validation->envmemcache());

        $this->writeNewEnvironmentFileWith('MEMCACHED_PERSISTENT_ID', $request->get('MEMCACHED_PERSISTENT_ID'));
        $this->writeNewEnvironmentFileWith('MEMCACHED_USERNAME', $request->get('MEMCACHED_USERNAME'));
        $this->writeNewEnvironmentFileWith('MEMCACHED_PASSWORD', $request->get('MEMCACHED_PASSWORD'));
        $this->writeNewEnvironmentFileWith('MEMCACHED_HOST', $request->get('MEMCACHED_HOST'));
        $this->writeNewEnvironmentFileWith('MEMCACHED_PORT', $request->get('MEMCACHED_PORT'));

        return response()->json(['status' => 'ok'], Response::HTTP_OK);
    }

    /**
     * @OA\Post(
     *     path="/setup/generate/key",
     *     tags={"setup"},
     *     description="Generate new key",
     *     security={{"bearer":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="response",
     *         @OA\JsonContent(
     *             title="response",
     *             type="object",
     *             example={},
     *         )
     *     ),
     * )
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|Response
     */
    public function generateKey()
    {
        try {
            $key = 'base64:'.base64_encode(random_bytes(
                app()['config']['app.cipher'] == 'AES-128-CBC' ? 16 : 32
            ));
            $this->writeNewEnvironmentFileWith('APP_KEY', $key);

            return response()->json(['status' => 'ok'], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'status'    => 'error',
                'message'   => $e->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *     path="/setup/generate/jwt",
     *     tags={"setup"},
     *     description="Generate new JWT key",
     *     security={{"bearer":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="response",
     *         @OA\JsonContent(
     *             title="response",
     *             type="object",
     *             example={},
     *         )
     *     ),
     * )
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|Response
     */
    public function generateJwt()
    {
        try {
            Artisan::call('jwt:secret', [
                '--force' => true,
            ]);

            return response()->json(['status' => 'ok'], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'status'    => 'error',
                'message'   => $e->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *     path="/setup/migrate",
     *     tags={"setup"},
     *     description="Migrate database",
     *     security={{"bearer":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="response",
     *         @OA\JsonContent(
     *             title="response",
     *             type="object",
     *             example={},
     *         )
     *     ),
     * )
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|Response
     */
    public function migrate()
    {
        try {
            Artisan::call('migrate', array('--force' => true));

            return response()->json(['status' => 'ok'], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'status'    => 'error',
                'message'   => $e->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *     path="/setup/seed",
     *     tags={"setup"},
     *     description="Seed database",
     *     security={{"bearer":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="response",
     *         @OA\JsonContent(
     *             title="response",
     *             type="object",
     *             example={},
     *         )
     *     ),
     * )
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|Response
     */
    public function seed()
    {
        try {
            Artisan::call('db:seed', array('--force' => true));

            return response()->json(['status' => 'ok'], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'status'    => 'error',
                'message'   => $e->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *     path="/setup/swagger",
     *     tags={"setup"},
     *     description="Swagger",
     *     security={{"bearer":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="response",
     *         @OA\JsonContent(
     *             title="response",
     *             type="object",
     *             example={},
     *         )
     *     ),
     * )
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|Response
     */
    public function swagger()
    {
        try {
            Artisan::call('swagger-lume:generate');

            return response()->json(['status' => 'ok'], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'status'    => 'error',
                'message'   => $e->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *     path="/setup/user",
     *     tags={"setup"},
     *     description="Create new user",
     *     security={{"bearer":{}}},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *           mediaType="application/x-www-form-urlencoded",
     *           @OA\Schema(
     *               type="object",
     *               @OA\Property(
     *                  property="name",
     *                  type="string"
     *               ),
     *               @OA\Property(
     *                  property="email",
     *                  type="string"
     *               ),
     *               @OA\Property(
     *                  property="password",
     *                  type="string"
     *               ),
     *           ),
     *       )
     *   ),
     *     @OA\Response(
     *         response=200,
     *         description="response",
     *         @OA\JsonContent(
     *             title="response",
     *             type="object",
     *             example={},
     *         )
     *     ),
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function user(Request $request)
    {
        $this->validate($request, $this->validation->user());

        /** @var User $user */
        $user = User::create([
            'name'      => $request->name,
            'password'  => Hash::make($request->password),
            'email'     => $request->email,
        ]);

        return response()->json([
            'user' => $user,
        ]);
    }

    /**
     * @OA\Post(
     *     path="/setup/env/microservices",
     *     tags={"setup"},
     *     description="Env for microservices",
     *     security={{"bearer":{}}},
     *   @OA\RequestBody(
     *       @OA\MediaType(
     *           mediaType="application/x-www-form-urlencoded",
     *           @OA\Schema(
     *               type="object",
     *               @OA\Property(
     *                  property="service",
     *                  type="string",
     *                  example="wallet",
     *               ),
     *               @OA\Property(
     *                  property="data",
     *                  type="string",
     *                  example={"MS_WALLET_EMAIL":"email@gmail.com","MS_WALLET_URL":"URL","MS_WALLET_PASSWORD":"PASS"}
     *               ),
     *           ),
     *       )
     *   ),
     *     @OA\Response(
     *         response=200,
     *         description="response",
     *         @OA\JsonContent(
     *             title="response",
     *             type="object",
     *             example={},
     *         )
     *     ),
     * )
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|Response
     */
    public function microservices(Request $request)
    {
        $this->validate($request, $this->validation->microservices());
        $service    = strtoupper($request->service);
        $data       = @json_decode($request->data, true);

        $this->writeNewEnvironmentFileWith('MS_' . $service . '_URL', ($data['MS_' . $service . '_URL'] ?? ''));
        $this->writeNewEnvironmentFileWith('MS_' . $service . '_EMAIL', ($data['MS_' . $service . '_EMAIL'] ?? ''));
        $this->writeNewEnvironmentFileWith('MS_' . $service . '_PASSWORD', ($data['MS_' . $service . '_PASSWORD'] ?? ''));

        return response()->json(['status' => 'ok'], Response::HTTP_OK);
    }

    /**
     * @OA\Post(
     *     path="/setup/env/lock",
     *     tags={"setup"},
     *     description="Lock for next updates",
     *     security={{"bearer":{}}},
     *     @OA\Response(
     *         response=200,
     *         description="response",
     *         @OA\JsonContent(
     *             title="response",
     *             type="object",
     *             example={},
     *         )
     *     ),
     * )
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|Response
     */
    public function envlock()
    {
        $this->writeNewEnvironmentFileWith('SETUP_LOCK', "1");

        return response()->json(['status' => 'ok'], Response::HTTP_OK);
    }

    /**
     * @param string $key
     * @param string|null $val
     */
    protected function writeNewEnvironmentFileWith(string $key, string $val = null)
    {
        $envContent = file_get_contents(base_path('.env'));

        if (preg_match($this->keyReplacementPattern($key), $envContent)) {
            file_put_contents(base_path('.env'), preg_replace(
                $this->keyReplacementPattern($key),
                $key . '=' . $val,
                $envContent,
            ));

            return;
        }

        file_put_contents(base_path('.env'), $envContent . "\r\n" . $key . '=' . $val);
    }

    /**
     * @param string $key
     * @return string
     */
    protected function keyReplacementPattern(string $key): string
    {
        $exists = env($key);

        if ($exists === true) {
            $exists = "true";
        }

        if ($exists === false) {
            $exists = "false";
        }

        if (preg_match('/ /', $exists)) {
            $exists = '"' . $exists . '"';
        }

        $escaped = preg_quote('=' . $exists, '/');

        $key        = preg_replace('/\"/', '\"', $key);
        $escaped    = preg_replace('/\"/', '\"', $escaped);

        return "/^" . $key . "{$escaped}/m";
    }
}
