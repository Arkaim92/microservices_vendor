<?php

namespace WTP\Microservices\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Determine if the user is logged in to any of the given guards.
     *
     * @param \Illuminate\Http\Request $request
     * @param array $guards
     * @return void
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function authenticate($request, array $guards)
    {
        $configToken = config('microservices.current.token');
        $token = $request->header('Authorization', null);
        if ($configToken && $token) {
            $token = str_replace('Bearer ', '', $token);
            if ($token === $configToken) {
                return;
            }
        }

        $this->unauthenticated($request, $guards);
    }
}
