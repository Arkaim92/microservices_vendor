<?php

namespace WTP\Microservices\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\ProvidesConvenienceMethods;

abstract class BaseRequestValidation
{

    use ProvidesConvenienceMethods {
        ProvidesConvenienceMethods::validate as _validate;
    }

    protected Request $request;

    protected array $validated;

    /**
     * Validate the given request.
     *
     * @param Request $request
     *
     * @throws ValidationException
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->validated = $this->validate();
    }

    /**
     * Validate the request with the rules.
     *
     * @return array
     *
     * @throws ValidationException
     */
    public function validate(): array
    {
        return $this->_validate(
            $this->request,
            $this->rules(),
            $this->messages(),
            $this->customAttribute()
        );
    }

    public function validated(): array
    {
        return $this->validated;
    }

    public function request(): Request
    {
        return $this->request;
    }

    abstract protected function rules(): array;

    protected function messages(): array
    {
        return [];
    }

    protected function customAttribute(): array
    {
        return [];
    }

}
