<?php

namespace WTP\Microservices\Http\Requests\SetupProject;

class RequestSetupValidation
{
    public function envdb(): array
    {
        return [
            'DB_CONNECTION' => ['required', 'string'],
            'DB_HOST'       => ['required', 'string'],
            'DB_PORT'       => ['required', 'string'],
            'DB_DATABASE'   => ['required', 'string'],
            'DB_USERNAME'   => ['required', 'string'],
            'DB_PASSWORD'   => ['required', 'string'],
        ];
    }

    public function envmemcache(): array
    {
        return [
            'MEMCACHED_PERSISTENT_ID'   => ['required', 'string'],
            'MEMCACHED_USERNAME'        => ['required', 'string'],
            'MEMCACHED_PASSWORD'        => ['required', 'string'],
            'MEMCACHED_HOST'            => ['required', 'string'],
            'MEMCACHED_PORT'            => ['required', 'string'],
        ];
    }

    public function user(): array
    {
        return [
            'name'      => 'required|min:3',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:3',
        ];
    }

    public function microservices(): array
    {
        return [
            'service' => ['required', 'string'],
            'data'    => ['required', 'string'],
        ];
    }
}
