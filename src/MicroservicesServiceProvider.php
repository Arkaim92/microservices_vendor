<?php

namespace WTP\Microservices;

use Illuminate\Support\ServiceProvider;
use WTP\Microservices\Console\Commands\CreateUser;

class MicroservicesServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/microservices.php', 'microservices'
        );
    }

    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/api.php');

        if ($this->app->runningInConsole()) {
            $this->commands([
                CreateUser::class
            ]);
        }
    }
}
