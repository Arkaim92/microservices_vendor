<?php

namespace WTP\Microservices\QueryBuilders;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

abstract class BaseBuilder
{

    protected Model $model;
    protected Request $request;
    protected Builder $builder;
    protected array $allowedWith = [];
    protected array $properties;

    /**
     * BaseBuilder constructor.
     * @param Request $request
     * @param Model $model
     */
    public function __construct(Request $request, Model $model)
    {
        $this->request = $request;
        $this->model = $model;
    }

    public function process($validated)
    {
        $this->beforeProcess();

        $this->buildFromValidated($validated, $this->properties);

        return $this->afterProcess();
    }

    protected function buildFromValidated($validated, $rules): void
    {
        foreach ($rules as $key => $rule) {
            if (!isset($validated[$key])) {
                continue;
            }

            if (isset($rule['type'], $rule['field'])) {
                if (isset($rule['operator'])) {
                    $this->builder->{$rule['type']}($rule['field'], $rule['operator'], $validated[$key]);
                } else {
                    $this->builder->{$rule['type']}($rule['field'], $validated[$key]);
                }
                continue;
            }

            if (isset($rule['function'], $rule['param'])) {
                $this->{$rule['function']}($validated[$rule['param']]);
                continue;
            }
        }
    }

    protected function validateAndFilterWith(): array
    {
        if (!isset($this->request->with) || !is_array($this->request->with)) {
            return [];
        }

        return array_merge(array_intersect($this->request->with, $this->allowedWith));
    }

    protected function paginateAndReturnCollection()
    {
        if (!isset($this->request->per_page)) {
            return $this->builder->paginate($this->model->getPerPage());
        }

        if ($this->request->per_page === 'all') {
            return $this->builder->get();
        }

        return $this->builder->paginate((int)$this->request->per_page);
    }

    protected function validateOrderDirection(?string $direction): string
    {
        if (!$direction) {
            return 'ASC';
        }

        if (in_array(strtoupper($direction), ['ASC', 'DESC'], true)) {
            return $direction;
        }

        return 'ASC';
    }

    protected function validateOrderColumn(string $column)
    {
        if (in_array($column, $this->model->getFillable(), true)) {
            return $column;
        }

        return false;
    }

    protected function order(): void
    {
        if (!isset($this->request->order)) {
            $this->builder->latest();
            return;
        }

        $direction = $this->validateOrderDirection($this->request->order['direction']);
        $column = $this->validateOrderColumn($this->request->order['column']);

        if (!$direction || !$column) {
            $this->builder->latest();
            return;
        }

        $this->builder->orderBy($column, $direction);
    }

    protected function beforeProcess(): void
    {
        $this->builder = $this->model->newQuery()->with($this->validateAndFilterWith());
    }

    protected function afterProcess()
    {
        $this->order();

        return $this->paginateAndReturnCollection();
    }

}
