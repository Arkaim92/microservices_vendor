<?php

namespace WTP\Microservices\Services\Rest;

use Illuminate\Http\Request;

class CustomerAuthService extends RestClient
{
    protected $name = 'customer';

    private const URL_LOGIN = '/customers/login';
    private const URL_REGISTER = '/customers/register';
    private const URL_FORGOT_PASSWORD = '/customers/forgot-password';
    private const URL_RESET_PASSWORD = '/customers/reset-password';

    /**
     * @param string $email Can be customer's login or email
     * @param string $password
     * @param int|null|string $google_2fa_code Google 2 fa code in case we got error code 422 with parameter 'google_2fa_code' than this code must be asked
     * @return mixed
     * @throws
     */
    public function loginCustomer(string $email, string $password, $google_2fa_code = null)
    {
        return $this->sendRequest(self::URL_LOGIN, Request::METHOD_POST, array_filter([
            'email' => $email,
            'password' => $password,
            'google_2fa_code' => $google_2fa_code ? (string)$google_2fa_code : null // Convert code to string
        ]));
    }

    /**
     * Register a new customer.
     *
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function registerCustomer(array $data)
    {
        $response = $this->sendRequest(self::URL_REGISTER, Request::METHOD_POST, $data);

        return $response->data ?? null;
    }

    /**
     * Send reset password email to customer
     * @param $email
     * @param $reset_link_template
     * @return mixed
     * @throws \Exception
     */
    public function forgotPassword($email, $reset_link_template)
    {
        return $this->sendRequest(self::URL_FORGOT_PASSWORD, Request::METHOD_POST, [
            'email' => $email,
            'reset_link_template' => $reset_link_template
        ]);
    }

    /**
     * Reset password method
     * @param $email
     * @param $password
     * @param $password_confirmation
     * @param $token
     * @return mixed
     * @throws \Exception
     */
    public function resetPassword($email, $password, $password_confirmation, $token)
    {
        return $this->sendRequest(self::URL_RESET_PASSWORD, Request::METHOD_POST, [
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $password_confirmation,
            'token' => $token
        ]);
    }

}
