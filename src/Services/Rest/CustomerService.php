<?php


namespace WTP\Microservices\Services\Rest;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CustomerService extends RestClient
{
    private const URL_CUSTOMERS = '/customers';
    private const URL_CUSTOMER = '/customers/{customer}';
    private const URL_GOOGLE_2FA_CODE = '/customers/{customer}/google2fa';
    private const URL_CUSTOMER_RANK = '/customers/{customer}/rank';
    private const URL_CUSTOMER_TOTALS = '/customers/totals';
    private const URL_CUSTOMER_SEND_EMAIL = '/customers/send-email';
    private const URL_CUSTOMER_BLOCK = '/customers/{id}/block';
    private const URL_CUSTOMER_UNBLOCK = '/customers/{id}/unblock';
    private const URL_CUSTOMER_COUNT_POTENTIAL_MULTIACCOUNT = '/potential-multiaccounts/count';

    protected $name = 'customer';

    /**
     * @param string $customer_id
     * @param array $with return customer with relations: partners, referrals, partner, licence,
     * @return mixed
     * @throws \Exception
     */
    public function find(string $customer_id, array $with = null)
    {
        $customer = $this->sendRequest($this->buildUrl(self::URL_CUSTOMER, ['customer' => $customer_id]), Request::METHOD_GET, [
            'with' => $with
        ]);
        return $customer->data ?? null;
    }

    /**
     * Get list of customers only with pagination
     * @param array $data
     * @param array $with
     * @return object
     * @throws \Exception
     */
    public function getCustomers(array $data = null, array $with = null): object
    {
        return $this->sendRequest(self::URL_CUSTOMERS, Request::METHOD_GET, array_filter(array_merge($data, [
            'with' => $with
        ])));
    }

    /**
     * @param $customerId
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function getReferrals($customerId, array $data = [])
    {
        return $this->sendRequest('/customers/' . $customerId . '/referrals', Request::METHOD_GET, $data);
    }

    /**
     * @param $customerId
     * @return mixed
     * @throws \Exception
     */
    public function getReferralsStatistic($customerId)
    {
        return $this->sendRequest('/customers/' . $customerId . '/referrals/statistic');
    }

    /**
     * Get details by customer's rank
     * @param $customerId
     * @return mixed
     * @throws \Exception
     */
    public function getCustomerRankDetails($customerId)
    {
        return $this->sendRequest($this->buildUrl(self::URL_CUSTOMER_RANK, ['customer' => $customerId]))->data;
    }


    /**
     * Get partner by my_id
     * @param $my_id
     * @return |null
     * @throws \Exception
     */
    public function getPartner($my_id)
    {
        $response = $this->getCustomers([
            'my_id' => $my_id
        ]);
        return $response->data ? current($response->data) : null;
    }

    /**
     * @param $key
     * @param null $date
     * @return |null
     * @throws \Exception
     */
    public function getTotalByKey($key, $date = null)
    {
        $cache = Cache::get('customersTotals');
        if (!$cache) {
            $cache = $this->getTotals($date);
        }
        return $cache->{$key} ?? null;
    }

    /**
     * Get customer totals
     * @param $date
     * @return mixed
     * @throws \Exception
     */
    public function getTotals($date)
    {
        return Cache::remember('customersTotals', getCacheLifetime('totals', 'c'), function () use ($date) {
            $response = $this->sendRequest(self::URL_CUSTOMER_TOTALS, Request::METHOD_GET, [
                'date' => $date
            ]);
            return $response->data ?? null;
        });
    }

    /**
     * Update customer
     * @param $customer_id
     * @param $data
     * @return |null
     * @throws \Exception
     */
    public function update($customer_id, $data)
    {
        return $this->sendRequest($this->buildUrl(self::URL_CUSTOMER, ['customer' => $customer_id]), Request::METHOD_PUT, $data);
    }

    /**
     * @param $customer_id
     * @return mixed
     * @throws \Exception
     */
    public function generateGoogle2FAData($customer_id)
    {
        return $this->sendRequest($this->buildUrl(self::URL_GOOGLE_2FA_CODE, ['customer' => $customer_id]));
    }

    /**
     * @param $customer_id
     * @param null $secret_code
     * @return mixed
     * @throws \Exception
     */
    public function enableGoogle2FAData($customer_id, $secret_code)
    {
        return $this->updateGoogle2FAData($customer_id, [
            'google_2fa' => true,
            'google_2fa_code' => $secret_code
        ]);
    }

    /**
     * @param $customer_id
     * @param null $secret_code
     * @return mixed
     * @throws \Exception
     */
    public function disableGoogle2FAData($customer_id)
    {
        return $this->updateGoogle2FAData($customer_id, [
            'google_2fa' => false
        ]);
    }

    /**
     * Update google 2 fa
     * @param $customer_id
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    protected function updateGoogle2FAData($customer_id, $data)
    {
        return $this->sendRequest($this->buildUrl(self::URL_GOOGLE_2FA_CODE, ['customer' => $customer_id]), Request::METHOD_PUT, $data);
    }

    /**
     * Send email notification to customer
     * @param $customerId
     * @param $type
     * @param array $data
     * @param boolean $skipNotVerified
     * @param int $delay
     * @return mixed
     * @throws \Exception
     */
    public function sendEmailNotification($customerId, $type, $data = [], $skipNotVerified = false, $delay = 0)
    {
        return $this->sendRequest(self::URL_CUSTOMER_SEND_EMAIL, Request::METHOD_POST, [
            'customer_id' => $customerId,
            'type' => $type,
            'data' => $data,
            'skip_not_verified' => $skipNotVerified,
            'delay' => $delay
        ]);
    }

    /**
     * Block the customer by the given ID.
     *
     * @param string $id ID of a customer
     * @return bool
     * @throws \Exception
     */
    public function blockCustomer(string $id)
    {
        $url = $this->buildUrl(self::URL_CUSTOMER_BLOCK, compact('id'));
        $this->sendRequest($url, Request::METHOD_PATCH);

        return true;
    }

    /**
     * Unblock the customer by the given ID.
     *
     * @param string $id ID of a customer
     * @return bool
     * @throws \Exception
     */
    public function unblockCustomer(string $id)
    {
        $url = $this->buildUrl(self::URL_CUSTOMER_UNBLOCK, compact('id'));
        $this->sendRequest($url, Request::METHOD_PATCH);

        return true;
    }

    /**
     * Get count of potential multiaccount customers.
     *
     * @return int
     * @throws \Exception
     */
    public function getCountPotentialMultiaccountCustomers(): int
    {
        $response = $this->sendRequest(self::URL_CUSTOMER_COUNT_POTENTIAL_MULTIACCOUNT, Request::METHOD_GET);

        return $response->data;
    }
}
