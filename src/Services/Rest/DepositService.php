<?php

namespace WTP\Microservices\Services\Rest;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Throwable;
use WTP\Microservices\DataModels\Deposit;
use WTP\Microservices\DataModels\Transaction;

class DepositService extends RestClient
{
    /** @var string $name */
    protected $name = 'deposit';

    /**
     * Gets a list of deposits, filtered by an array of filters.
     *
     * @param array $filters ['active' => true, 'created_at' => ['operator' => '>', 'value' => '2020-11-25 12:41:38']]
     * @return Deposit[]
     */
    public function getDeposits(array $filters = []): array
    {
        /** @var Deposit[] $deposits */
        $deposits = [];

        try {
            $response = $this->sendRequest("/deposits", Request::METHOD_GET, [], $filters);

            foreach ($response->data as $depositData) {
                $deposits[] = new Deposit($depositData);
            }
        } catch (Throwable $e) {
            Log::critical('Deposits can not be fetched', [
                'filters'   => $filters,
                'message'   => $e->getMessage(),
                'code'      => $e->getCode(),
                'trace'     => $e->getTrace(),
            ]);
        }

        return $deposits;
    }

    /**
     * Creates a new deposit from the data array.
     *
     * @param array $data
     * @return Deposit|null
     */
    public function createDeposit(array $data = []): ?Deposit
    {
        /** @var Deposit $deposit */
        $deposit = null;

        try {
            $response = $this->sendRequest("/deposits", Request::METHOD_POST, $data);
            $deposit = new Deposit($response->data);
        } catch (Throwable $e) {
            Log::critical('Deposit can not be created', [
                'data'      => $data,
                'message'   => $e->getMessage(),
                'code'      => $e->getCode(),
                'trace'     => $e->getTrace(),
            ]);
        }

        return $deposit;
    }

    /**
     * @param array|null $customerIds
     * @param bool|null $active
     * @return mixed
     * @throws Exception
     */
    public function getTotalInvested(array $customerIds = null, bool $active = null)
    {
        return $this->sendRequest('/deposits/totals/invested', Request::METHOD_GET, [
            'customer_ids' => $customerIds,
            'active' => $active
        ])->data;
    }

    /**
     * Updates the deposit by id with a data array.
     *
     * @param string $id of the deposit
     * @param array $data
     * @return Deposit|null
     */
    public function updateDeposit(string $id, array $data): ?Deposit
    {
        /** @var Deposit $deposit */
        $deposit = null;

        try {
            $response = $this->sendRequest("/deposits/{$id}", Request::METHOD_PUT, $data);
            $deposit = new Deposit($response->data);
        } catch (Throwable $e) {
            Log::critical('Deposit can not be updated', [
                'id'        => $id,
                'data'      => $data,
                'message'   => $e->getMessage(),
                'code'      => $e->getCode(),
                'trace'     => $e->getTrace(),
            ]);
        }

        return $deposit;
    }

    /**
     * Updates the deposits filtered by array with a data array.
     *
     * @param array $data
     * @param array $filters
     * @return Deposit[]
     */
    public function updateManyDeposits(array $data, array $filters = []): array
    {
        /** @var Deposit[] $deposits */
        $deposits = [];

        try {
            $response = $this->sendRequest("/deposits/update/many", Request::METHOD_PUT, $data, $filters);
            foreach ($response->data as $depositData) {
                $deposits[] = new Deposit($depositData);
            }
        } catch (Throwable $e) {
            Log::critical('Deposits can not be updated', [
                'data'      => $data,
                'filters'   => $filters,
                'message'   => $e->getMessage(),
                'code'      => $e->getCode(),
                'trace'     => $e->getTrace(),
            ]);
        }

        return $deposits;
    }

    /**
     * Accrues the deposit balance by id.
     *
     * @param string $id of the deposit
     * @return Transaction|null
     */
    public function accrueDeposit(string $id): ?Transaction
    {
        /** @var Transaction $transaction */
        $transaction = null;

        try {
            $response = $this->sendRequest("/deposits/{$id}/accrue", Request::METHOD_POST);
            $transaction = new Transaction($response->data);
        } catch (Throwable $e) {
            Log::critical('Deposit can not be accrued', [
                'id'        => $id,
                'message'   => $e->getMessage(),
                'code'      => $e->getCode(),
                'trace'     => $e->getTrace(),
            ]);
        }

        return $transaction;
    }

    /**
     * Exempts the deposit by id.
     *
     * @param string $id of the deposit
     * @param array $data
     * @return Transaction|null
     */
    public function exemptDeposit(string $id, array $data): ?Transaction
    {
        /** @var Transaction $transaction */
        $transaction = null;

        try {
            $response = $this->sendRequest("/deposits/{$id}/exempt", Request::METHOD_DELETE, $data);
            $transaction = new Transaction($response->data);
        } catch (Throwable $e) {
            Log::critical('Deposit can not be exempted', [
                'id'        => $id,
                'data'      => $data,
                'message'   => $e->getMessage(),
                'code'      => $e->getCode(),
                'trace'     => $e->getTrace(),
            ]);
        }

        return $transaction;
    }

    /**
     * Closes the deposit by id.
     *
     * @param string $id of the deposit
     * @param array $data
     * @return Transaction|null
     */
    public function closeDeposit(string $id, array $data): ?Transaction
    {
        /** @var Transaction $transaction */
        $transaction = null;

        try {
            $response = $this->sendRequest("/deposits/{$id}/close", Request::METHOD_DELETE, $data);
            $transaction = new Transaction($response->data);
        } catch (Throwable $e) {
            Log::critical('Deposit can not be closed', [
                'id'        => $id,
                'data'      => $data,
                'message'   => $e->getMessage(),
                'code'      => $e->getCode(),
                'trace'     => $e->getTrace(),
            ]);
        }
        return $transaction;
    }
}
