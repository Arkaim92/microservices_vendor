<?php

namespace WTP\Microservices\Services\Rest;

use Illuminate\Http\Request;

class PotentialMultiaccountService extends RestClient
{
    private const URL_POTENTIAL_MULTIACCOUNTS = '/potential-multiaccounts';
    private const URL_POTENTIAL_MULTIACCOUNT = '/potential-multiaccounts/{id}';

    /** @var string Microservice name */
    protected $name = 'customer';

    /**
     * Get list of customers which are potential multiaccount.
     *
     * @param array $data
     * @return object
     * @throws \Exception
     */
    public function get(array $data = []): object
    {
        return $this->sendRequest(
            self::URL_POTENTIAL_MULTIACCOUNTS,
            Request::METHOD_GET,
            $data,
        );
    }

    /**
     * Find a potential multiaccount customer by the given ID.
     *
     * @param string $id
     * @return object Standard class object
     * @throws \Exception
     */
    public function find(string $id): object
    {
        $url = $this->buildUrl(self::URL_POTENTIAL_MULTIACCOUNT, compact('id'));
        $response = $this->sendRequest($url, Request::METHOD_GET);

        return $response->data;
    }

    /**
     * Check the customer for potential multiaccounting by the given ID.
     *
     * @param string $id
     * @return bool
     * @throws \Exception When the request to the microservice will fail
     */
    public function check(string $id): bool
    {
        $url = $this->buildUrl(self::URL_POTENTIAL_MULTIACCOUNT, compact('id'));
        $this->sendRequest($url, Request::METHOD_PATCH);

        return true;
    }

    /**
     * Delete potential multiaccount relations from the customer by the given ID.
     *
     * @param string $id
     * @return bool
     * @throws \Exception When the request to the microservice will fail
     */
    public function delete(string $id): bool
    {
        $url = $this->buildUrl(self::URL_POTENTIAL_MULTIACCOUNT, compact('id'));
        $this->sendRequest($url, Request::METHOD_DELETE);

        return true;
    }
}
