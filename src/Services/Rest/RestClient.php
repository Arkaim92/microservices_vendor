<?php

namespace WTP\Microservices\Services\Rest;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * Class RestClient
 * @package WTP\Microservices\Services\Rest
 */
abstract class RestClient
{
    /** @var int How many times to repeat if an error occurred */
    public const TIMES_REPEAT = 5;

    /** @var Client $client */
    protected $client;

    /** @var string $name */
    protected $name;

    /** @var array $config */
    protected $config;

    /** @var string $baseUrl */
    protected $baseUrl;

    /** @var array */
    public $params = [
        'headers' => [
            'Content-Type'  => 'application/json',
            'Accept'        => 'application/json',
        ],
        'timeout' => 0,
        'http_errors' => false // Not throw exceptions in case we retrieved HTTP protocol errors (i.e., 4xx and 5xx responses)
    ];

    /**
     * RestClient constructor.
     * @param array $options
     * @throws Exception
     */
    public function __construct(array $options = [])
    {
        $this->config = config('microservices.' . $this->name, []);

        if (empty($this->config)) {
            throw new Exception('There is not a configuration for the service');
        }

        $this->baseUrl = rtrim($this->config['url'], '/');
        $this->client = new Client($options);

        $this->params['verify'] = strtolower(env('APP_ENV', 'local')) == 'production';
    }

    /**
     * @param  string  $method Method name (/news, /faqs)
     * @param  string  $type Method type (GET, POST, PUT, PATCH, DELETE)
     * @param  array  $data During GET request query data must be taken from data
     * @param array $query Only during (POST, PUT, PATCH, DELETE) to create filters
     * @return mixed
     * @throws Exception
     */
    public function sendRequest(string $method, string $type = Request::METHOD_GET, array $data = [], array $query = [])
    {
        $this->setAuthorization(); // Set JsonWebToken
        $params = array_merge($this->params, [$this->getDataKeyByMethodType($type) => $data]);
        if (!empty($query)) {
            $params[RequestOptions::QUERY] = $query;
        }

        for ($step = 0; $step < self::TIMES_REPEAT; $step++) { // Repeated times
            try {
                $response = $this->client->request($type, $this->baseUrl . $method, $params);
            } catch (Throwable $e) {
                throw new Exception(sprintf('%s failed. Error %s', get_class($this), $e->getMessage()));
            }

            // If token has been expired
            if (
                $response->getStatusCode() === Response::HTTP_UNAUTHORIZED
                && $method !== '/auth/login'
                && $step === 0 // check only on first step
            ) {
                $this->setAuthorization();
                $params['headers']['Authorization'] = $this->params['headers']['Authorization'];
                continue;
            } elseif ($response->getStatusCode() === Response::HTTP_SERVICE_UNAVAILABLE) { // Try more times if service is not available
                continue;
            }

            if ($response->getStatusCode() === Response::HTTP_UNAUTHORIZED) {
                Log::error('RE SIGN IN DOES NOT WORK', [
                    'step' => $step
                ]);
            }

            break; // break if another type of requests
        }

        return $this->parseResponse($response, $method);
    }

    /**
     * Parse response
     * @param  ResponseInterface $response
     * @param  $method
     * @return mixed
     * @throws Exception
     */
    public function parseResponse($response, $method = null)
    {
        $decodedResponse = json_decode($response->getBody()->getContents());
        if (!in_array($response->getStatusCode(), [Response::HTTP_OK, Response::HTTP_CREATED, Response::HTTP_ACCEPTED])) {
            if ($response->getStatusCode() === Response::HTTP_UNPROCESSABLE_ENTITY) {
                Log::warning('CUSTOMER SERVICE VALIDATION ERRORS: ', (array) $decodedResponse->errors);
                throw ValidationException::withMessages((array)$decodedResponse->errors);
            }

            $message = sprintf(
                '%s failed. Status %s for method %s',
                get_class($this),
                $response->getStatusCode(),
                $method
            );

            Log::error('CUSTOMER: SERVICE ' . $this->name . ' ERROR:' . $message, [
                'response' => $decodedResponse
            ]);

            throw new Exception($decodedResponse->message ?? __('Internal server error'));
        }

        return $decodedResponse;
    }

    /**
     * Set json web token
     *
     * @param  bool  $refresh
     * @throws Exception
     */
    protected function setAuthorization()
    {
        $token = $this->config['token'];

        if (empty($token)) {
            throw new Exception('Token can not be set');
        }

        // Set authorization for request
        $this->params['headers']['Authorization'] = 'Bearer ' . $token;
    }

    /**
     * @param  string  $method_type Method type (GET, POST, PUT, PATCH, DELETE)
     * @return string  Data key for the request method type
     */
    protected function getDataKeyByMethodType(string $method_type): string
    {
        return Request::METHOD_GET === $method_type
            ? RequestOptions::QUERY
            : RequestOptions::JSON;
    }

    /**
     * Build url
     * @param string $url
     * @param array $parameters
     * @return string
     */
    protected function buildUrl(string $url, array $parameters): string
    {
        foreach ($parameters as $key => $value) {
            $url = str_replace('{' . $key . '}', $value, $url);
        }

        return $url;
    }
}
