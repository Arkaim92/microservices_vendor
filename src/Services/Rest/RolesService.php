<?php


namespace WTP\Microservices\Services\Rest;


class RolesService extends RestClient
{
    protected $name = 'customer';

    private const URL_ROLES = '/roles';

    /**
     * Get list of all roles
     * @return mixed
     * @throws \Exception
     */
    public function getRoles()
    {
        return $this->sendRequest(self::URL_ROLES);
    }
}
