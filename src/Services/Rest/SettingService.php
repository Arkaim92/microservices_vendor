<?php

namespace WTP\Microservices\Services\Rest;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Throwable;

class SettingService extends RestClient
{
    private const CACHE_LIFETIME = 1800; // 30 minute = 1800 seconds

    /** @var string $name */
    protected $name = 'setting';

    /** @var \Illuminate\Contracts\Cache\Repository|mixed|null */
    protected $settings = null;

    public function __construct(array $options = [])
    {
        parent::__construct($options);

        $this->settings = Cache::get('settings.global');
        $lastUpdatedTime = Cache::get('settings.last_update_time');
        if (
            empty($this->settings) || empty($lastUpdatedTime)
            || Carbon::parse($lastUpdatedTime)->diffInSeconds(Carbon::now()) > self::CACHE_LIFETIME
        ) { // Synchronize settings
            $this->synchronize();
            if (empty($this->settings)) {
                Log::critical('Empty global settings config. Settings can not be synchronized');
            }
        }
    }

    /**
     * Get rate from settings
     * @param string $from
     * @param string $to
     * @return float
     */
    public function rate(string $from, string $to): float
    {
        $from = strtolower($from);
        $to = strtolower($to);

        if ($from === $to) {
            return 1;
        }

        $rate = (float)$this->get($from . '_to_' . $to, 0);
        if ($rate == 0.0) {
            $rate_back = (float)$this->get($to . '_to_' . $from, 0);
            if ($rate_back > 0) {
                $rate = 1 / $rate_back;
            }
        }

        return $rate;
    }

    /**
     * Get value from settings.
     *
     * @param $key
     * @param null $default
     * @param bool $cache if !== true then a real request would be processed
     * @return float|string|null
     */
    public function get($key, $default = null, bool $cache = true)
    {
        if ($cache !== true) {
            try {
                $response = $this->sendRequest("/settings/{$key}", Request::METHOD_GET);
                $this->settings->{$key} = $response->data;
                Cache::forever('settings.global', $this->settings);
            } catch (Exception $e) {
                // Log errors
                Log::critical('Setting can not be fetch', [
                    'key' => $key,
                    'message' => $e->getMessage(),
                    'code' => $e->getCode(),
                    'trace' => $e->getTrace(),
                ]);
            }
        }
        return $this->settings->{$key} ?? $default;
    }

    /**
     * Synchronize settings
     * Settings will be synchronized automatically
     * @return boolean
     */
    public function synchronize(): bool
    {
        try {
            // Save to cache even this request is failed
            Cache::forever('settings.last_update_time', Carbon::now()->toDateTimeString());
            $response = $this->sendRequest('/settings', Request::METHOD_GET);
            Cache::forever('settings.global', $this->settings = $response->data);
            return true;
        } catch (Throwable $e) {
            // Log errors
            Log::critical('Settings can not be synchronised', [
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'trace' => $e->getTrace(),
            ]);

            return false;
        }
    }

    /**
     * Set a new setting value (updates or creates a setting).
     *
     * @param string $key
     * @param string|null $value
     * @return bool
     */
    public function update(string $key, string $value = null): bool
    {
        return $this->set("/settings/{$key}", $key, $value);
    }

    /**
     * Increase a setting value with a float number (updates or creates a setting).
     *
     * @param string $key
     * @param float|null $value
     * @return bool
     */
    public function increase(string $key, float $value = null): bool
    {
        return $this->set("/settings/{$key}/increase", $key, $value);
    }

    /**
     * Decrease a setting value with a float number (updates or creates a setting).
     *
     * @param string $key
     * @param float|null $value
     * @param float|null $limit param to limit a minimum value
     * @return bool
     */
    public function decrease(string $key, float $value = null, float $limit = null): bool
    {
        return $this->set("/settings/{$key}/decrease", $key, $value, $limit);
    }

    /**
     * Set a new setting value.
     * Generalization of the update()/increase()/decrease() methods with a required URI param.
     *
     * @param string $uri
     * @param string $key
     * @param string|null $value
     * @param float|null $limit
     * @return bool
     */
    private function set(
        string $uri,
        string $key,
        string $value = null,
        float $limit = null
    ): bool
    {
        $result = false;
        $requestData = [
            'key' => $key,
            'value' => $value,
        ];

        if ($limit) {
            $requestData['limit'] = $limit;
        }

        try {
            $response = $this->sendRequest($uri, Request::METHOD_POST, $requestData);

            // if response is ok
            if ($response->data) {
                // update settings cache with a new value
                $this->settings->{$key} = $response->data;
                Cache::forever('settings.global', $this->settings);

                $result = true;
            }
        } catch (Exception $e) {
            // Log errors
            Log::critical('Setting can not be updated', [
                'setting' => $key . ' = ' . $value,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'trace' => $e->getTrace(),
            ]);
        }

        return $result;
    }
}
