<?php


namespace WTP\Microservices\Services\Rest;


use Illuminate\Http\Request;

class TelegramService extends RestClient
{
    public const NOTIFICATION_BOT = 'notification_bot';

    /** @var string $name */
    protected $name = 'telegram';

    protected string $bot;

    /**
     * TelegramService constructor.
     * @param array $options
     * @throws \Exception
     */
    public function __construct(array $options = [])
    {
        parent::__construct($options);
        $this->setBot(self::NOTIFICATION_BOT);
    }

    /**
     * @param string $bot
     * @return $this
     */
    public function setBot(string $bot)
    {
        $this->bot = $bot;
        return $this;
    }


    /**
     * @param array $data [customer_id: '', 'template_code: '', 'template_variables': ['amount': 100]]
     * @return mixed
     * @throws \Exception
     */
    public function sendNotification(array $data)
    {
        return $this->sendNotifications([$data]);
    }

    /**
     * Send multiple notifications
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function sendNotifications(array $data)
    {
        return $this->sendRequest(
            $this->buildUrl('/notifications/{bot}', ['bot' => $this->bot]),
            Request::METHOD_POST,
            $data
        );
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function getTemplates(array $data)
    {
        return $this->sendRequest('/templates', Request::METHOD_GET, $data);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function storeTemplate(array $data)
    {
        return $this->sendRequest('/templates', Request::METHOD_POST, $data);
    }

    /**
     * @param integer $type_id
     * @param array $translations [[language: en, text: English], [language: ru, text: Русский]]
     * @return mixed
     * @throws \Exception
     */
    public function updateTemplate(int $type_id, array $translations)
    {
        return $this->sendRequest('/templates/' . $type_id, Request::METHOD_PUT, $translations);
    }

    /**
     * Get all template's types
     * @return array
     * @throws \Exception
     */
    public function getTemplateTypes()
    {
        $response = $this->sendRequest('/template-types');
        return $response->data ?? [];
    }

    /**
     * @param integer $type_id
     * @return mixed
     * @throws \Exception
     */
    public function deleteTemplateType(int $type_id)
    {
        return $this->sendRequest('/template-types/' . $type_id, Request::METHOD_DELETE);
    }

    /**
     * Send telegram webhook
     * @param string $token
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function webhook(string $token, array $data)
    {
        return $this->sendRequest(
            $this->buildUrl('/webhook/{token}', ['token' => $token]),
            Request::METHOD_POST,
            $data
        );
    }

    /**
     * Show customer
     * @param string $customerId
     * @return mixed
     * @throws \Exception
     */
    public function getCustomer(string $customerId)
    {
        $response = $this->sendRequest(
            $this->buildUrl('/customers/{bot}/{customer}', ['bot' => $this->bot, 'customer' => $customerId])
        );
        return $response->data ?? null;
    }

    /**
     * Delete customer for specific bot
     * @param string $customerId
     * @return mixed
     * @throws \Exception
     */
    public function deleteCustomer(string $customerId)
    {
        return $this->sendRequest(
            $this->buildUrl('/customers/{bot}/{customer}', ['bot' => $this->bot, 'customer' => $customerId]),
            Request::METHOD_DELETE
        );
    }
}
