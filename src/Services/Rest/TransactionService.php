<?php

namespace WTP\Microservices\Services\Rest;

use Illuminate\Http\Request;
use LogicException;

class TransactionService extends RestClient
{
    public const TRANSACTION_TYPE_WITHDRAW = 'withdraw';
    public const TRANSACTION_TYPE_TRANSFER = 'transfer';

    public const TRANSACTION_STATUS_CREATED = 1;
    public const TRANSACTION_STATUS_APPROVED = 2;
    public const TRANSACTION_STATUS_CONFIRMED_BY_EMAIL = 3;
    public const TRANSACTION_STATUS_REJECTED = 4;
    public const TRANSACTION_STATUS_ERROR = 5;

    /** @var string Name of the microservice */
    protected $name = 'transaction';

    /**
     * Get a list of the transaction types.
     *
     * @return array Array of standard class objects
     * @throws \Exception When the request to the microservice will fail
     */
    public function getTransactionTypes(): array
    {
        $response = $this->sendRequest('/transaction-types', Request::METHOD_GET);

        return $response->data;
    }

    /**
     * Store a new transaction type data.
     *
     * @param array $data
     * @return object Standard class object with an id property
     * @throws \Exception When the request to the microservice will fail
     */
    public function storeTransactionType(array $data): object
    {
        $response = $this->sendRequest('/transaction-types', Request::METHOD_POST, $data);

        return $response->data;
    }

    /**
     * Find a transaction type by the given ID.
     *
     * @param string $id
     * @return object Standard class object
     * @throws \Exception When the request to the microservice will fail
     */
    public function findTransactionType(string $id): object
    {
        $url = $this->buildUrl('/transaction-types/{id}', compact('id'));
        $response = $this->sendRequest($url, Request::METHOD_GET);

        return $response->data;
    }

    /**
     * Update the transaction type by the given data.
     *
     * @param string $id
     * @param array $data
     * @return bool
     * @throws \Exception When the request to the microservice will fail
     */
    public function updateTransactionType(string $id, array $data): bool
    {
        if (empty($data)) {
            throw new LogicException('At least one field must be filled.');
        }

        $url = $this->buildUrl('/transaction-types/{id}', compact('id'));
        $this->sendRequest($url, Request::METHOD_PATCH, $data);

        return true;
    }

    /**
     * Delete a transaction type by the given ID.
     *
     * @param string $id
     * @return bool
     * @throws \Exception When the request to the microservice will fail
     */
    public function deleteTransactionType(string $id): bool
    {
        $url = $this->buildUrl('/transaction-types/{id}', compact('id'));
        $this->sendRequest($url, Request::METHOD_DELETE);

        return true;
    }

    /**
     * Get a list of the transaction statuses.
     *
     * @return array Array of standard class objects
     * @throws \Exception When the request to the microservice will fail
     */
    public function getTransactionStatuses(): array
    {
        $response = $this->sendRequest('/transaction-statuses', Request::METHOD_GET);

        return $response->data;
    }

    /**
     * Store a new transaction type data.
     *
     * @param array $data
     * @return object Standard class object with an id property
     * @throws \Exception When the request to the microservice will fail
     */
    public function storeTransactionStatus(array $data): object
    {
        $response = $this->sendRequest('/transaction-statuses', Request::METHOD_POST, $data);

        return $response->data;
    }

    /**
     * Find a transaction type by the given ID.
     *
     * @param int $id
     * @return object Standard class object
     * @throws \Exception When the request to the microservice will fail
     */
    public function findTransactionStatus(int $id): object
    {
        $url = $this->buildUrl('/transaction-statuses/{id}', compact('id'));
        $response = $this->sendRequest($url, Request::METHOD_GET);

        return $response->data;
    }

    /**
     * Update the transaction type by the given data.
     *
     * @param int $id
     * @param array $data
     * @return bool
     * @throws \Exception When the request to the microservice will fail
     */
    public function updateTransactionStatus(int $id, array $data): bool
    {
        if (empty($data)) {
            throw new LogicException('At least one field must be filled.');
        }

        $url = $this->buildUrl('/transaction-statuses/{id}', compact('id'));
        $this->sendRequest($url, Request::METHOD_PATCH, $data);

        return true;
    }

    /**
     * Delete a transaction type by the given ID.
     *
     * @param int $id
     * @return bool
     * @throws \Exception When the request to the microservice will fail
     */
    public function deleteTransactionStatus(int $id): bool
    {
        $url = $this->buildUrl('/transaction-statuses/{id}', compact('id'));
        $this->sendRequest($url, Request::METHOD_DELETE);

        return true;
    }

    /**
     * Get a list of the transactions.
     *
     * @param array $filter Filter the list
     * @return object
     * @throws \Exception When the request to the microservice will fail
     */
    public function getTransactions(array $filter): object
    {
        return $this->sendRequest('/transactions', Request::METHOD_GET, $filter);
    }

    /**
     * Store a new transaction type data.
     *
     * @param array $data
     * @return object Standard class object with an id property
     * @throws \Exception When the request to the microservice will fail
     */
    public function storeTransaction(array $data): object
    {
        $response = $this->sendRequest('/transactions', Request::METHOD_POST, $data);

        return $response->data;
    }

    /**
     * Find a transaction type by the given ID.
     *
     * @param string $id
     * @return object Standard class object
     * @throws \Exception When the request to the microservice will fail
     */
    public function findTransaction(string $id): object
    {
        $url = $this->buildUrl('/transactions/{id}', compact('id'));
        $response = $this->sendRequest($url, Request::METHOD_GET);

        return $response->data;
    }

    /**
     * Update the transaction type by the given data.
     *
     * @param string $id
     * @param array $data
     * @return bool
     * @throws \Exception When the request to the microservice will fail
     */
    public function updateTransaction(string $id, array $data): bool
    {
        if (empty($data)) {
            throw new LogicException('At least one field must be filled.');
        }

        $url = $this->buildUrl('/transactions/{id}', compact('id'));
        $this->sendRequest($url, Request::METHOD_PATCH, $data);

        return true;
    }

    /**
     * Delete a transaction type by the given ID.
     *
     * @param string $id
     * @return bool
     * @throws \Exception When the request to the microservice will fail
     */
    public function deleteTransaction(string $id): bool
    {
        $url = $this->buildUrl('/transactions/{id}', compact('id'));
        $this->sendRequest($url, Request::METHOD_DELETE);

        return true;
    }

    /**
     * Store a new transaction any exchange data.
     *
     * For the type field you must use part after the 'exchange_' prefix.
     *
     * @param array $data
     * @return object Standard class object with an id property
     * @throws \Exception When the request to the microservice will fail
     */
    public function storeTransactionExchangeAny(array $data): object
    {
        $response = $this->sendRequest('/transactions/exchange-any', Request::METHOD_POST, $data);

        return $response->data;
    }

    /**
     * Store a new transaction any exchanger data.
     *
     * For the type field you must use part after the 'exchanger_' prefix.
     *
     * @param array $data
     * @return object Standard class object with an id property
     * @throws \Exception When the request to the microservice will fail
     */
    public function storeTransactionExchangerAny(array $data): object
    {
        $response = $this->sendRequest('/transactions/exchanger-any', Request::METHOD_POST, $data);

        return $response->data;
    }

    /**
     * Get the transactions affiliate.
     *
     * @param array $filter Filter the list
     * @return object Standard class objects
     * @throws \Exception When the request to the microservice will fail
     */
    public function getTransactionsAffiliate(array $filter): object
    {
        $response = $this->sendRequest('/transactions/affiliate', Request::METHOD_GET, $filter);

        return $response->data;
    }

    /**
     * Approve a transaction.
     *
     * @param string $id
     * @return bool
     * @throws \Exception When the request to the microservice will fail
     */
    public function transactionApprove(string $id): bool
    {
        $url = $this->buildUrl('/transactions/{id}/approve', compact('id'));
        $this->sendRequest($url, Request::METHOD_PATCH);

        return true;
    }

    /**
     * Get a list of the withdraws.
     *
     * @param array $filter Filter the list
     * @return object Array of standard class objects
     * @throws \Exception When the request to the microservice will fail
     */
    public function getWithdraws(array $filter): object
    {
        return $this->sendRequest('/withdraws', Request::METHOD_GET, $filter);
    }

    /**
     * Store a new withdraw type data.
     *
     * @param array $data
     * @return object Standard class object with an id property
     * @throws \Exception When the request to the microservice will fail
     */
    public function storeWithdraw(array $data): object
    {
        $response = $this->sendRequest('/withdraws/create', Request::METHOD_POST, $data);

        return $response->data;
    }

    /**
     * Find a withdraw type by the given ID.
     *
     * @param string $id
     * @return object Standard class object
     * @throws \Exception When the request to the microservice will fail
     */
    public function findWithdraw(string $id): object
    {
        $url = $this->buildUrl('/withdraws/{id}', compact('id'));
        $response = $this->sendRequest($url, Request::METHOD_GET);

        return $response->data;
    }

    /**
     * Update the withdraw type by the given data.
     *
     * @param string $id
     * @param array $data
     * @return bool
     * @throws \Exception When the request to the microservice will fail
     */
    public function updateWithdraw(string $id, array $data): bool
    {
        if (empty($data)) {
            throw new LogicException('At least one field must be filled.');
        }

        $url = $this->buildUrl('/withdraws/{id}', compact('id'));
        $this->sendRequest($url, Request::METHOD_PATCH, $data);

        return true;
    }

    /**
     * Delete a withdraw type by the given ID.
     *
     * @param string $id
     * @return bool
     * @throws \Exception When the request to the microservice will fail
     */
    public function deleteWithdraw(string $id): bool
    {
        $url = $this->buildUrl('/withdraws/{id}', compact('id'));
        $this->sendRequest($url, Request::METHOD_DELETE);

        return true;
    }

    /**
     * Store a new withdraw swap data.
     *
     * @param array $data
     * @return object Standard class object with an id property
     * @throws \Exception When the request to the microservice will fail
     */
    public function withdrawSwap(array $data): object
    {
        $response = $this->sendRequest('/withdraws/swap', Request::METHOD_POST, $data);

        return $response->data;
    }

    /**
     * Confirm a withdraw.
     *
     * @param string $id
     * @return bool
     * @throws \Exception When the request to the microservice will fail
     */
    public function withdrawConfirm(string $id): bool
    {
        $url = $this->buildUrl('/withdraws/{id}/confirm', compact('id'));
        $this->sendRequest($url, Request::METHOD_PATCH);

        return true;
    }

    /**
     * Confirm a withdraw by the code.
     *
     * @param string $code Code of a withdraw
     * @return bool
     * @throws \Exception When the request to the microservice will fail
     */
    public function withdrawConfirmByCode(string $code): bool
    {
        $url = $this->buildUrl('/withdraws/{code}/confirm-by-code', compact('code'));
        $this->sendRequest($url, Request::METHOD_PATCH);

        return true;
    }

    /**
     * Approve a withdraw.
     *
     * @param string $id
     * @return object Standard class object
     * @throws \Exception When the request to the microservice will fail
     */
    public function withdrawApprove(string $id): object
    {
        $url = $this->buildUrl('/withdraws/{id}/approve', compact('id'));

        return $this->sendRequest($url, Request::METHOD_PATCH)->data;
    }

    /**
     * Approve manually a withdraw.
     *
     * @param string $id
     * @return object With message and data key. The data key has another amount and symbol key.
     * @throws \Exception When the request to the microservice will fail
     */
    public function withdrawApproveManually(string $id): object
    {
        $url = $this->buildUrl('/withdraws/{id}/approve-manually', compact('id'));

        return $this->sendRequest($url, Request::METHOD_PATCH);
    }

    /**
     * Reject a withdraw.
     *
     * @param string $id
     * @param array $data
     * @return bool
     * @throws \Exception When the request to the microservice will fail
     */
    public function withdrawReject(string $id, array $data = []): bool
    {
        $url = $this->buildUrl('/withdraws/{id}/reject', compact('id'));
        $this->sendRequest($url, Request::METHOD_PATCH, $data);

        return true;
    }

    /**
     * Get sums of withdraws grouped by the given payment systems and currencies.
     *
     * @param array $payment_systems
     * @param array $currencies
     * @return array
     * @throws \Exception When the request to the microservice will fail
     */
    public function withdrawSums(array $payment_systems, array $currencies): array
    {
        $response = $this->sendRequest(
            '/withdraws/sums',
            Request::METHOD_GET,
            compact('payment_systems', 'currencies')
        );

        return $response->data ?? [];
    }

    /**
     * Store a new transfer data.
     *
     * @param array $data
     * @return bool
     * @throws \Exception When the request to the microservice will fail
     */
    public function storeTransfer(array $data): bool
    {
        $this->sendRequest('/transfers/create', Request::METHOD_POST, $data);

        return true;
    }

    /**
     * Confirm a transfer.
     *
     * @param array $data
     * @return bool
     * @throws \Exception When the request to the microservice will fail
     */
    public function transferConfirm(array $data): bool
    {
        $this->sendRequest('/transfers/confirm', Request::METHOD_PATCH, $data);

        return true;
    }

    /**
     * Confirm a transfer by the code.
     *
     * @param string $code Code of a transfer
     * @return bool
     * @throws \Exception When the request to the microservice will fail
     */
    public function transferConfirmByCode(string $code): bool
    {
        $url = $this->buildUrl('transfers/{code}/confirm-by-code', compact('code'));
        $this->sendRequest($url, Request::METHOD_PATCH);

        return true;
    }

    /**
     * Get total deposited + withdrew
     * @return mixed
     * @throws \Exception
     */
    public function getTotalDepositedWithdrew()
    {
        return $this->sendRequest('/totals/deposited-withdrew');
    }

    /**
     * @param null $days number of last days
     * @return mixed
     * @throws \Exception
     */
    public function getAdminMoneyTrafficStatistic($days = null)
    {
        return $this->sendRequest('/totals/money-traffic', Request::METHOD_GET, [
            'days' => $days
        ]);
    }

    /**
     * @param null $days number of last days
     * @return mixed
     * @throws \Exception
     */
    public function getAdminLicensesTrafficStatistic($days = null)
    {
        return $this->sendRequest('/totals/licences', Request::METHOD_GET, [
            'days' => $days
        ]);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getGeneralStatistic()
    {
        return $this->sendRequest('/totals/general');
    }

    /**
     * @param string $customerId
     * @param array|null $types
     * @param bool|null $approved
     * @return mixed
     * @throws \Exception
     */
    public function getTotalTransactionsByCustomer(string $customerId, array $types = null, bool $approved = null)
    {
        return $this->sendRequest('/totals/customer', Request::METHOD_GET, array_filter([
            'customer_id' => $customerId,
            'types' => $types,
            'approved' => $approved
        ]));
    }
}
