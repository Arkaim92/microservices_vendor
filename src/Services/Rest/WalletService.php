<?php

namespace WTP\Microservices\Services\Rest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WalletService extends RestClient
{
    private const GET_WALLETS = '/wallets'; // Directly from database
    private const GET_CUSTOMER_WALLETS = '/wallets/customer'; // This method uses cache
    private const URL_WALLET = '/wallets/{wallet}';
    private const POST_WALLET_REGISTER = '/wallets/register';
    private const PATCH_WALLET_INCREASE = '/wallets/{wallet}/increase';
    private const PATCH_WALLET_DECREASE = '/wallets/{wallet}/decrease';
    private const DELETE_WALLET_FILTERS = '/wallets/delete';

    public $name = 'wallet';

    /**
     * @param string $customerId
     * @param string $currencyCode
     * @param string|null $paymentSystemCode
     * @return array|object
     * @throws \Exception
     */
    public function getCustomerWallet(
        string $customerId,
        string $currencyCode,
        string $paymentSystemCode = null
    )
    {
        $wallets = $this->getWalletsByCustomer([
            'customer_id' => $customerId,
            'currency_code' => $currencyCode,
            'payment_system_code' => $paymentSystemCode
        ]);
        if (is_array($wallets) && count($wallets) === 1) {
            return current($wallets);
        }
        return $wallets;
    }

    /**
     * @param string $customerId
     * @param string $currencyId
     * @param string|null $paymentSystemId
     * @return array|object
     * @throws \Exception
     */
    public function getCustomerWalletByIds(
        string $customerId,
        string $currencyId,
        string $paymentSystemId = null
    )
    {
        $wallets = $this->getWalletsByCustomer([
            'customer_id' => $customerId,
            'currency_id' => $currencyId,
            'payment_system_id' => $paymentSystemId
        ]);
        if (is_array($wallets) && count($wallets) === 1) {
            return current($wallets);
        }
        return $wallets;
    }

    /**
     * @param array $filters
     * @return mixed
     * @throws \Exception
     */
    public function getWalletsByCustomer(array $filters)
    {
        try {
            $response = $this->sendRequest(self::GET_CUSTOMER_WALLETS, Request::METHOD_GET, $filters);
            return $response->data;
        } catch (\Throwable $e) {
            Log::error('Customer: Wallets can not be retrieved');
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @param $filters
     * @param array|null $with
     * @return mixed
     * @throws \Exception
     */
    public function getWallets($filters, array $with = null)
    {
        return $this->sendRequest(self::GET_WALLETS, Request::METHOD_GET, array_merge($filters, ['with' => $with]));
    }

    /**
     * Create new empty wallet
     * @param $customerId
     * @param $currencyId
     * @param $paymentSystemId
     * @param $address
     * @return mixed
     * @throws \Exception
     */
    public function create($customerId, $currencyId, $paymentSystemId, $address = null)
    {
        $response = $this->sendRequest(self::GET_WALLETS, Request::METHOD_POST, [
            'customer_id' => $customerId,
            'currency_id' => $currencyId,
            'payment_system_id' => $paymentSystemId,
            'address' => $address
        ]);
        return $response ?? null;
    }

    /**
     * @param $customer_id
     * @return mixed
     * @throws \Exception
     */
    public function registerWalletsForCustomer($customer_id)
    {
        return $this->sendRequest(self::POST_WALLET_REGISTER, Request::METHOD_POST, [
            'customer_id' => $customer_id
        ]);
    }

    /**
     * Update wallet
     * @param string $wallet_id
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function update(string $wallet_id, array $data)
    {
        $response = $this->sendRequest($this->buildUrl(self::URL_WALLET, [
            'wallet' => $wallet_id
        ]), Request::METHOD_PUT, $data);
        return $response->data ?? null;
    }

    /**
     * @param string $customerId
     * @return mixed
     * @throws \Exception
     */
    public function deleteByCustomerId(string $customerId)
    {
        return $this->delete(['customer_id' => $customerId]);
    }

    /**
     * Method to delete wallet by filters
     * @param $filters
     * @return mixed
     * @throws \Exception
     */
    public function deleteByFilter($filters)
    {
        return $this->sendRequest(self::DELETE_WALLET_FILTERS, Request::METHOD_DELETE, [], $filters);
    }

    /**
     * @param $wallet_id
     * @param $amount
     * @param bool $accrue_amount_to_partner
     * @param float $accrue_type
     * @return mixed
     * @throws \Exception
     */
    public function increase($wallet_id, $amount, $accrue_amount_to_partner = null, $accrue_type = null)
    {
        return $this->sendRequest($this->buildUrl(self::PATCH_WALLET_INCREASE, ['wallet' => $wallet_id]), Request::METHOD_PATCH, [
            'amount' => $amount,
            'accrue_amount_to_partner' => $accrue_amount_to_partner,
            'accrue_type' => $accrue_type
        ]);
    }

    /**
     * @param $walletId
     * @param $amount
     * @param null $accrue_amount_to_partner
     * @param null $accrue_type
     * @param array $accrue_type_data
     * @return mixed
     * @throws \Exception
     */
    public function decrease(
        $walletId, $amount, $accrue_amount_to_partner = null, $accrue_type = null, $accrue_type_data = []
    )
    {
        return $this->sendRequest($this->buildUrl(self::PATCH_WALLET_DECREASE, ['wallet' => $walletId]), Request::METHOD_PATCH, [
            'amount' => $amount,
            'accrue_amount_to_partner' => $accrue_amount_to_partner,
            'accrue_type' => $accrue_type,
            'accrue_type_data' => $accrue_type_data
        ]);
    }

    /**
     * Find wallet by id
     * @param $walletId
     * @param array $with Contains relations which must be include for wallet. By default currency is always returned
     * @return mixed
     * @throws \Exception
     */
    public function find($walletId, $with = null)
    {
        $response = $this->sendRequest(
            $this->buildUrl(self::URL_WALLET, ['wallet' => $walletId]),
            Request::METHOD_GET,
            array_filter([
                'with' => $with
            ])
        );
        return $response->data ?? null;
    }
}
