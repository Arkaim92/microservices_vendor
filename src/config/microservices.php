<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Microservices configuration
    |--------------------------------------------------------------------------
    |
    | Put all the settings here that the application needs to access other microservices.
    | This can include base urls, request headers, etc.
    |
    */

    'current' => [
        'token' => env('MS_CURRENT_API_TOKEN', null)
    ],

    'customer' => [
        'url' => env('MS_CUSTOMER_URL', 'http://127.0.0.1/api/v1'),
        'token' => env('MS_CUSTOMER_TOKEN')
    ],

    'setting' => [
        'url' => env('MS_SETTING_URL', 'http://127.0.0.1/api/v1'),
        'token' => env('MS_SETTING_TOKEN')
    ],

    'deposit' => [
        'url' => env('MS_DEPOSIT_URL', 'http://127.0.0.1/api/v1'),
        'token' => env('MS_DEPOSIT_TOKEN')
    ],

    'transaction' => [
        'url' => env('MS_TRANSACTION_URL', 'http://127.0.0.1/api/v1'),
        'token' => env('MS_TRANSACTION_TOKEN')
    ],

    'wallet' => [
        'url' => env('MS_WALLET_URL', 'http://127.0.0.1/api/v1'),
        'token' => env('MS_WALLET_TOKEN')
    ],

    'telegram' => [
        'url' => env('MS_TELEGRAM_URL', 'http://127.0.0.1/api/v1'),
        'token' => env('MS_TELEGRAM_TOKEN')
    ],

    'content' => [
        'url' => env('MS_CONTENT_URL', 'http://127.0.0.1/api/v1'),
        'token' => env('MS_CONTENT_TOKEN')
    ],

    'statistic' => [
        'url' => env('MS_STATISTIC_URL', 'http://127.0.0.1/api/v1'),
        'token' => env('MS_STATISTIC_TOKEN')
    ],
];
