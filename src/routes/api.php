<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// Api methods
use Illuminate\Support\Facades\Route;
use Laravel\Lumen\Routing\Router;

Route::group([
    'middleware' => 'auth'
], function (Router $router) {
    // Base namespace
    $router->group([
        'middleware' => 'auth',
        'namespace' => 'WTP\Microservices\Http\Controllers\Base',
    ], function () use ($router) {
        $router->group(['prefix' => '/api/v1/setup'], function () use ($router) {
            $router->post('env/db', ['uses' => 'SetupProjectController@envdb']);
            $router->post('env/memcache', ['uses' => 'SetupProjectController@envmemcache']);
            $router->post('generate/key', ['uses' => 'SetupProjectController@generateKey']);
            $router->post('generate/jwt', ['uses' => 'SetupProjectController@generateJwt']);
            $router->post('migrate', ['uses' => 'SetupProjectController@migrate']);
            $router->post('seed', ['uses' => 'SetupProjectController@seed']);
            $router->post('swagger', ['uses' => 'SetupProjectController@swagger']);
            $router->post('user', ['uses' => 'SetupProjectController@user']);
            $router->post('env/microservices', ['uses' => 'SetupProjectController@microservices']);
            $router->post('env/lock', ['uses' => 'SetupProjectController@envlock']);
        });
    });
});
